package web;

import java.io.IOException;


public class dvcaCommunicate 
{
	private static String std_parent = "0";
	private static String key_id = "3,1.3.6.1.5.5.7.3.3";

    public static void sendCertRequest(String url, String dn, String contact, byte[] key) throws IOException 
    {        	
    	System.out.println("Sending request for certificate...");
    	
    	if(!url.endsWith("/"))
    		url += "/";
   		HTTPPost cerpost = new HTTPPost(url + "ops/request");
   		cerpost.addParameter("parent_ca", std_parent);
   		cerpost.addParameter("subject_dn", dn);
   		cerpost.addParameter("key_usage", key_id);
   		cerpost.addParameter("contact", contact);
   		cerpost.addByteContent("pubkey_file", key);
   		cerpost.post();

   		System.out.println("Request for certificate has been sent...");
    }
}
