package web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class HTTPPost 
{
	private URLConnection connection;
	private OutputStream connectionstream;

	String line = "---------------------------";

	private void boundary() throws IOException 
	{
	    connectionstream.write("--".getBytes());
	    connectionstream.write(line.getBytes());
	}
	
	public HTTPPost(String url)
	{
	    try {
			connection = new URL(url).openConnection();
			connection.setDoOutput(true);
		    connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + line);
		    connectionstream = connection.getOutputStream();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addParameter(String param, String val)
	{
		try {
			boundary();
			connectionstream.write(("\nContent-Disposition: form-data; name=\"" + param + "\"\r\n\r\n" + val + "\r\n").getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addFileParameter(String param, String filename)
	{
		try {
		InputStream input = new FileInputStream(new File(filename));
		boundary();
		connectionstream.write(("\nContent-Disposition: form-data; name=\"" + param + "\";").getBytes());
		connectionstream.write((" filename=\"" + filename + "\"\r\n").getBytes());
		
		String type = URLConnection.guessContentTypeFromName(filename);
		type = (type == null ? "application/octet-stream" : type);
		
		connectionstream.write(("Content-Type: " + type + "\r\n\r\n").getBytes());
		
		byte[] buffer = new byte[2048]; //maximum expected key length;
		
		int read = input.read(buffer, 0, 2048);
		connectionstream.write(buffer, 0, read);
		connectionstream.flush();			
		connectionstream.write(("\r\n").getBytes());
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	
	public void addByteContent(String param, byte[] bytecontent) throws IOException
	{
		boundary();
		connectionstream.write(("\nContent-Disposition: form-data; name=\"" + param + "\";").getBytes());
		connectionstream.write((" filename=\"keyfile\"\r\n").getBytes());
		connectionstream.write(("Content-Type: application/octet-stream\r\n\r\n").getBytes());
		
		connectionstream.write(bytecontent, 0, bytecontent.length);
		connectionstream.flush();			
		connectionstream.write(("\r\n").getBytes());
	}
	
	  public InputStream post() throws IOException 
	  {
		    boundary();
		    connectionstream.write(("--\r\n").getBytes());
		    connectionstream.close();
		    return connection.getInputStream();
	  }

}
