package security;

import java.security.Key;

import javax.crypto.Cipher;

import stai.Sign;

import emulator.CardEmulator;
import emulator.CardEmulatorException;

import iaik.pkcs.pkcs7.RSACipherProvider;

public class OurProvider extends RSACipherProvider
{
	private CardEmulator emu;
	
	private byte[] applet_id;
	
	public OurProvider(CardEmulator emu, byte[] appletID)
	{
		super();
		this.emu = emu;
		applet_id = appletID;
	}
	
	@Override
	public byte[] cipher(int mode, Key key, byte[] bytes)
	{				
		switch(mode)
		{
		case Cipher.ENCRYPT_MODE:
			try 
			{
				return Sign.getSignature(emu, bytes);
			} catch (CardEmulatorException e) 
			{
				e.printStackTrace();
			}
		case Cipher.DECRYPT_MODE:
			return null;
		default:
			return null;
		}
	}
}