package security;

import iaik.asn1.ASN1Object;
import iaik.asn1.ObjectID;
import iaik.asn1.structures.AlgorithmID;
import iaik.asn1.structures.Attribute;
import iaik.asn1.structures.ChoiceOfTime;
import iaik.pkcs.PKCS7CertList;
import iaik.pkcs.pkcs7.IssuerAndSerialNumber;
import iaik.pkcs.pkcs7.RSACipherProvider;
import iaik.pkcs.pkcs7.SignedData;
import iaik.pkcs.pkcs7.SignerInfo;
import iaik.x509.X509Certificate;

import java.io.File;
import java.io.FileInputStream;
import java.security.Key;

import javax.crypto.Cipher;

import stai.Sign;
import emulator.CardEmulator;
import emulator.CardEmulatorException;

class Cardling extends RSACipherProvider
{
	
	private CardEmulator emu;
	
	public Cardling(CardEmulator emu)
	{
		this.emu = emu;
	}
	
	public byte[] cipher(int mode, Key key, byte[] data)
	{
		switch(mode)
		{
		case Cipher.ENCRYPT_MODE:
			
			try 
			{			
				byte[] b =  Sign.getSignature(emu, data);
				return b;
			} catch (CardEmulatorException e) 
			{
				e.printStackTrace();
			}
			return null;
		default:
			return null;
		}
	}
}

public class SignedDataCreator
{
	public static SignedData createSignedData(CardEmulator emu, String certfile, byte[] bytes) throws Exception
	{		
		/*CardEmulator card = new CardEmulator();
		
		card.selectApplet(appletID);
		
		//KeyFactory kf = KeyFactory.getInstance("RSA");
		//RSAPublicKeySpec pubkeyspec= Utils.getPublicKey(card);
		
		//PublicKey pubkey = kf.generatePublic(pubkeyspec);
		
	    PKCS7CertList cert_list = new PKCS7CertList(new FileInputStream(new File(certfile)));
	    X509Certificate[] certificates = cert_list.getCertificateList();
	    
	    IssuerAndSerialNumber no = new IssuerAndSerialNumber(certificates[0]);
	    
	    SignerInfo info = new SignerInfo(no, AlgorithmID.sha, null);
	    
	   // info.setAuthenticatedAttributes(info_attribs);
	   // info.setRSACipherProvider(new OurProvider(card, appletID));
	    
	   // Attribute[] attributes = {new Attribute(ObjectID.messageDigest,
       //         new ASN1Object[] {new iaik.asn1.OCTET_STRING(digest)})};
	    
	    //Attribute[] info_attribs = {new Attribute(ObjectID.x509Certificate, 
		//		  new ASN1Object[]{certificates[0].toASN1Object()}),
	//new Attribute(ObjectID.signingTime,
	//			  new ASN1Object[]{new ChoiceOfTime().toASN1Object()})};
	    
	    MessageDigest sha = MessageDigest.getInstance("SHA");
	    sha.update(bytes);
	    byte[] digest = sha.digest();
	    
	    Attribute[] attributes = new Attribute[1];
	    
	   // attributes[0] = new Attribute(ObjectID.contentType, new ASN1Object[]
	   // 		{ObjectID.pkcs7_data});

	    		// signing time is now
	    //attributes[1] = new Attribute(ObjectID.signingTime, new ASN1Object[]
	    //		{new ChoiceOfTime().toASN1Object()});
	    
	    attributes[0] = new Attribute(ObjectID.messageDigest,
	    	       new ASN1Object[] {new iaik.asn1.OCTET_STRING(digest)});
	    
	   // info.setAuthenticatedAttributes(attributes);
	    
	    //byte[] signature = Sign.getSignature(card, DerCoder.encode(iaik.asn1.ASN.createSetOf(attributes,true)));
	    byte[] signature = Sign.getSignature(card, digest);
	    
	    info.setEncryptedDigest(signature);

	    //rsaSHA.update();
	    
	    SignedData signed = new SignedData(bytes, SignedData.IMPLICIT);
	    signed.setCertificates(certificates);
	    signed.addSignerInfo(info);
	    
	    card.disconnect();
	    
	    FileOutputStream fos = new FileOutputStream(new File("out.signed"));
	    signed.writeTo(fos);
	    fos.flush(); 
	    fos.close();   
	    
	    signed.verify(0);
	    
	    System.out.println("Created signed file" + "out.signed");*/
		
	    PKCS7CertList cert_list = new PKCS7CertList(new FileInputStream(new File(certfile)));
	    X509Certificate[] certificates = cert_list.getCertificateList();
	    
	    SignedData data = new SignedData(bytes, SignedData.IMPLICIT);
	    
	    IssuerAndSerialNumber no = new IssuerAndSerialNumber(certificates[0]);
	    
	    SignerInfo info = new SignerInfo(no, AlgorithmID.sha, null);
	    
	    Attribute[] attribs  = {new Attribute(ObjectID.contentType, new ASN1Object[]{ObjectID.pkcs7_data}),
	    						new Attribute(ObjectID.signingTime, new ASN1Object[]{new ChoiceOfTime().toASN1Object()})};
	    
	    info.setAuthenticatedAttributes(attribs);
	    
	    info.setRSACipherProvider(new Cardling(emu));
	    
	    data.addSignerInfo(info);
	    
	    data.setCertificates(certificates);
	    
	    return data;
	}
}
