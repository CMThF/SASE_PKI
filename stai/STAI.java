package stai;


public class STAI 
{
    private static final byte APPLET_AID[] = new byte[] {
    (byte)0x01, (byte)0x02, (byte)0x03, (byte)0x04,
    (byte)0x05, (byte)0x06, (byte)0x07, (byte)0x08,
    (byte)0x09, (byte)0x00, (byte)0x00
    };
    
    private static final byte NAME[] = new byte[] 
    {
    	(byte)'S', (byte)'T', (byte)'A', (byte)'I', (byte)'0'
    };
    
	private static Request request_;
	private static Import import_;
	private static Sign sign_;
	private static Verify verify_;
	
	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			printUsage();
			return;
		}
		
		init();
		
		try
		{
			if(args[0].equals("certificate-request"))
			{
				request_.requestCertificate(args[1]);
			}
			else if(args[0].equals("certificate-import"))
			{	
				import_.importCertificate(args[1]);
			}
			else if(args[0].equals("sign"))
			{
				sign_.signFile(args[1]);
			}
			else if(args[0].equals("verify"))
			{
				verify_.verifyFile(args[1]);
			}
			else
			{
				printUsage();
				return;
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private static void printUsage()
	{
		System.out.println("Invalid program invocation.");
		System.out.println("Available commands are:");
		System.out.println("certificate-request <url-to-dvca>");
		System.out.println("certificate-import <file-or-url>");
		System.out.println("verify <file>");
		System.out.println("sign <file>");
	}
	
	private static void init()
	{
		request_ = new Request(APPLET_AID, NAME);
		import_ = new Import(APPLET_AID);
		verify_ = new Verify();
		sign_ = new Sign(APPLET_AID);
	}
}