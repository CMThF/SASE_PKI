package stai;

import iaik.pkcs.PKCSParsingException;
import iaik.pkcs.pkcs7.SignedData;
import iaik.x509.X509Certificate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.SignatureException;

public class Verify 
{

	public  void verifyFile(String file) throws IOException, PKCSParsingException, SignatureException
	{
		File verifyfile = new File(file);
		
		FileInputStream input = new FileInputStream(verifyfile);
		
		SignedData verifydata = new SignedData(input);
		
		System.out.println("Loading certificates");
		
		X509Certificate[] certs = verifydata.getCertificates();
		
		System.out.println("Verifying certificates");
		
		for(int x = 0; x < verifydata.getSignerInfos().length; x++)
			verifydata.verify(x);
		
		System.out.println("Verified with success!");
	}

}
