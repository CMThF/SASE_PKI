package stai;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import web.dvcaCommunicate;

import emulator.CardEmulator;
import emulator.CardEmulatorException;

public class Request 
{
	private String common_name;
	private String organization;
	private String country;
	private String contact;
	
	private byte[] applet_id;
	
	private byte[] name;
	
	public Request(byte[] id, byte[] name)
	{
		applet_id = id;
		this.name = name;
	}
	
	public void readInfo() throws IOException
	{
		common_name = Utils.readString("Please enter preferred 'Common Name'");
		organization = Utils.readString("Please enter the name of your organization");
		country = Utils.readString("Please enter your country");
		contact = Utils.readString("Please enter contact information!");
	}
	
	public  void requestCertificate(String url) throws CardEmulatorException, IOException, NoSuchAlgorithmException, InvalidKeySpecException
	{
		System.out.println("Handling certificate request");
		
		CardEmulator card = new CardEmulator();
		
		Utils.tryInstall(card, applet_id, name);
		
		card.selectApplet(applet_id);
		
		readInfo();
		
		RSAPublicKeySpec spec = Utils.getPublicKey(card);
		
		KeyFactory keyfactory = KeyFactory.getInstance("RSA");
		
		PublicKey pub = keyfactory.generatePublic(spec);
		
		byte[] pubkey = pub.getEncoded();
		
		String dn = "CN=" + common_name + ", O=" + organization + ", C=" + country;

		dvcaCommunicate.sendCertRequest(url, dn, contact, pubkey);
		
		System.out.println("Request handling done");
	}

}
