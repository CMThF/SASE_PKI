package stai;

import iaik.pkcs.PKCS7CertList;
import iaik.x509.X509Certificate;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import emulator.CardEmulator;

public class Import 
{
	private byte[] applet_id;
	
	public static String import_directory = "imported";
	
	public Import(byte[] id)
	{
		applet_id = id;
	}
	
	private static String download(String destdir, String url) throws IOException
	{
		String[] parts = url.trim().split("/");
		
		String fname = parts[parts.length-1];
		
		String destfile_str = destdir + "/" + fname;
		
		URL src = new URL(url);
		URLConnection conn = src.openConnection();
		
		int datasize = conn.getContentLength();
		byte[] buffer = new byte[datasize];
		
		BufferedInputStream buff = new BufferedInputStream(conn.getInputStream());
		buff.read(buffer);
		
		File destfile = new File(destfile_str);
		destfile.createNewFile();
		
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destfile));
		out.write(buffer);
		out.flush();
		out.close();
		
		return destfile_str;
	}
	
	private static String fileCopy(String destdir, String file) throws IOException
	{
		String[] parts = file.trim().split("[\\\\/]");
		
		String fname = parts[parts.length-1];
		
		String destfile_str = destdir + "/" + fname;
		
		File dest = new File(destfile_str);
		File src  = new File(file);
		
		FileOutputStream out = new FileOutputStream(dest);
		FileInputStream in = new FileInputStream(src);
		
		byte[] buffer = new byte[50000];
		
		int read;
		while((read = in.read(buffer)) > 0)
		{
			out.write(buffer, 0, read);
		}
		out.flush();
		out.close();
		in.close();
		
		return destfile_str;
	}
	
	public void importCertificate(String urlorfile) throws Exception
	{
		System.out.println("Importing certificate...");
		
		File imported = new File(import_directory);
		File f = new File(urlorfile);
		
		if(!imported.exists())
		{
			System.out.println("Creating import directory...");
			
			if(!imported.mkdir())
			{
				throw new FileNotFoundException("Could not create directory for import");
			}
		}
		else if(!imported.isDirectory())
		{
			throw new FileNotFoundException("File path is not a directory");
		}
		
		String dest;
		if(f.exists())
		{
			System.out.println("Copying file...");
			dest = fileCopy(import_directory, urlorfile);
		}
		else //try url
		{
			System.out.println("Downloading file...");
			dest = download(import_directory, urlorfile);
		}
		
		System.out.println("Import complete!");
		
		PKCS7CertList pkcs7 = new PKCS7CertList(new FileInputStream(dest));
		X509Certificate[] certs = pkcs7.getCertificateList();
		
		System.out.println("Validating certificate...");
		
		certs[0].checkValidity();
		System.out.println("Comparing public keys...");
		
		PublicKey certkey = certs[0].getPublicKey();
		
		CardEmulator card = new CardEmulator();
		card.selectApplet(applet_id);
		RSAPublicKeySpec spec = Utils.getPublicKey(card);
		KeyFactory keyfactory = KeyFactory.getInstance("RSA");

		PublicKey pub = keyfactory.generatePublic(spec);
		
		for(int i = 0; i < pub.getEncoded().length; i++)
		{
			if(pub.getEncoded()[i] != certkey.getEncoded()[i])
			{
				throw new InvalidKeySpecException("Public key does not match key on secure token!");
			}
		}
		System.out.println("Certificate is valid!");
	}
}
