package stai;

import iaik.pkcs.pkcs7.SignedData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Vector;

import security.SignedDataCreator;
import emulator.CardEmulator;
import emulator.CardEmulatorException;

public class Sign 
{
	private byte[] applet_id;
	
	public static byte[] getSignature(CardEmulator emu, byte[] hash) throws CardEmulatorException 
    {
    	byte[] result = emu.exchange((byte)0x80, (byte)0x26, (byte)0x00, (byte)0x00, hash);
    	
    	return result;
    }
	
	public Sign(byte[] id)
	{
		applet_id = id;
	}
	
	public  void signFile(String file) throws Exception
	{
		File importdir = new File(Import.import_directory);
		
		if(!importdir.exists() || !importdir.isDirectory())
		{
			throw new FileNotFoundException("Could not locate directory for imported keys!");
		}
		
		String[] content = importdir.list();
		Vector<String> files = new Vector<String>();
		
		for(String s : content)
		{
			if(s.endsWith(".p7c"))
			{
				files.add(s);
			}
		}
		
		if(files.size() <= 0)
		{
			throw new FileNotFoundException("No suitable certificates found!");
		}
		
		int selected = 0;
		
		if(files.size() > 1)
		{
			System.out.println("The following certificate files could be located:");
		
			for(int i = 0; i < files.size(); i++)
			{
				System.out.println(i + ") " + files.get(i));
			}
			
			do
			{
				String selection = Utils.readString("Please choose a number between 0 and " + (files.size() -1));
				selected = Integer.parseInt(selection);
			}while(selected < 0 || selected >= files.size());
		}
		
		String certfile = Import.import_directory + "/" + files.get(selected);
		
		System.out.println("Using certificate file " + certfile);
		
		File sign_file = new File(file);
		if(!sign_file.exists())
		{
			throw new FileNotFoundException("The selected file is not available for signing");
		}
		
		System.out.println("Read file to buffer");
		
		byte[] buffer = new byte[50000];
		FileInputStream fstream = new FileInputStream(sign_file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream((int) sign_file.length());
		
		int read;
		while((read = fstream.read(buffer)) > 0)
		{
			baos.write(buffer, 0, read);
		}
		
		byte[] bytes = baos.toByteArray();
		
		CardEmulator emu = new CardEmulator();
		
		emu.selectApplet(applet_id);
		
		System.out.println("Create signed data, encrypt with JavaCard");
		
		SignedData signeddata = SignedDataCreator.createSignedData(emu, certfile, bytes);

		FileOutputStream signeddataout = new FileOutputStream(new File("out.sig"));
		
		System.out.println("Write signed output file");

		signeddata.writeTo(signeddataout);
	    
		signeddataout.flush();
		signeddataout.close();
		
		System.out.println("Signing done!");
		
		emu.disconnect();
	}

}
