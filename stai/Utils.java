package stai;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.spec.RSAPublicKeySpec;

import emulator.CardEmulator;
import emulator.CardEmulatorException;

public class Utils 
{
	public static void tryInstall(CardEmulator card, byte[] id, byte[] name)
	{
		System.out.println("Trying to install cardlet");
		
		try
		{
			card.installApplet(id, name);
			
			System.out.println("Cardlet installation succeeded");
		}
		catch(Exception ex)
		{
			//Nothing, it might just be because it's already installed.
			System.out.println("Cardlet installation failed (may be already installed)");
		}
	}
	
	/**
	 * 
	 * Based on Sase Tutorial
	 * 
	 * @param card
	 * @return
	 * @throws CardEmulatorException
	 */
    public static RSAPublicKeySpec getPublicKey(CardEmulator card) throws CardEmulatorException 
    {
    		byte[] buffer = card.exchange((byte)0x80, (byte)0x25, (byte)0x00, (byte)0x00, null);
       		int offset = 0;

       		/* Extract the modulus */
       		int modulus_len = extractInt16(buffer, offset);
       		offset += 2;

       		BigInteger modulus = extractBigInt(buffer, offset, modulus_len);
       		offset += modulus_len;

       		/* Extract the exponent */
       		int exponent_len = extractInt16(buffer, offset);
       		offset += 2;

       		BigInteger exponent = extractBigInt(buffer, offset, exponent_len);

       		/* Return the key specification */
       		return new RSAPublicKeySpec(modulus, exponent);
    }
	
    /**
     * Based on SASE tutorial
     * 
     * @param array
     * @param offset
     * @return
     */
    public static int extractInt16(byte[] array, int offset) 
    {
    	return (((int)array[offset] & 0xFF) << 8) |
	    ((int)array[offset + 1] & 0xFF);
    }
	
    /**
     * Based on SASE tutorial
     * 
     * @param array
     * @param offset
     * @param length
     * @return
     */
    public static BigInteger extractBigInt(byte[] array, int offset, int length) 
    {
    	byte[] slice = new byte[length];
    	System.arraycopy(array, offset, slice, 0, length);
    	return new BigInteger(1, slice);
    }
	
    public static String readString(String message) throws IOException
    {
    	String read = "";
    		
    	System.out.println("> " + message);
    	InputStreamReader reader = new InputStreamReader(System.in);
    	BufferedReader bf = new BufferedReader(reader);
    	
    	read = bf.readLine();
    	return read;
    }

}
