package emulator;

import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.Socket;

import com.sun.javacard.apduio.Apdu;
import com.sun.javacard.apduio.CadDevice;
import com.sun.javacard.apduio.CadClientInterface;
import com.sun.javacard.apduio.CadTransportException;

/**
 * Convenience interface to the CREF JavaCard emulator from SUN's JavaCard Development Kit.
 *
 * @author Johannes Winter <johannes.winter@iaik.tugraz.at>
 */
public final class CardEmulator
{
    /**
     * Default host for T0 prototcol emulation
     */
    private static final String T0_DEFAULT_HOST = "localhost";

    /**
     * Default socket port for the T0 protocol emulation
     */
    private static final int T0_DEFAULT_PORT = 9025;

    /**
     * The com.sun.javacard.apduio.CadClientInterface object used for low-level communication.
     */
    private CadClientInterface client;

    /**
     * The ATR send by the card emulator upon powerup.
     */
    private byte[] atr;

    /**
     * CLA byte for the INSTALL APDU
     */
    public final static byte APDU_INSTALL_CLA = (byte)0x80;

    /**
     * INS byte for the INSTALL APDU
     */
    public final static byte APDU_INSTALL_INS = (byte)0xB8;

    /**
     * P1 byte for the INSTALL APDU
     */
    public final static byte APDU_INSTALL_P1  = (byte)0x00;

    /**
     * P2 byte for the INSTALL APDU
     */
    public final static byte APDU_INSTALL_P2  = (byte)0x00;

    /**
     * CLA byte for the SELECT APDU
     */
    public final static byte APDU_SELECT_CLA = (byte)0x00;

    /**
     * INS byte for the SELECT APDU
     */
    public final static byte APDU_SELECT_INS = (byte)0xA4;

    /**
     * P1 byte for the SELECT APDU
     */
    public final static byte APDU_SELECT_P1  = (byte)0x04;

    /**
     * P2 byte for the SELECT APDU
     */
    public final static byte APDU_SELECT_P2  = (byte)0x00;

    /**
     * AID of the JavaCard installer applet
     */
    public static final byte[] INSTALLER_AID = new byte[] {
	(byte)0xa0, (byte)0x0, (byte)0x0, (byte)0x0, (byte)0x62, (byte)0x3, (byte)0x1, (byte)0x8, (byte)0x1
    };

    /**
     * Creates a new card emulator client and establishes a socket connection
     * to the local card emulator.
     *
     * Thist constructor uses the default port number (2605) and the local host
     * name ("localhost") for establishing the socket connection.
     */
    public CardEmulator()
	throws CardEmulatorException {
	this(T0_DEFAULT_HOST);
    }

    /**
     * Creates a new card emulator client and establisesh a socket connection
     * to the given card emulator.
     *
     * This constructor uses the default port number (9026) for establishing
     * the socket connection.
     *
     * @param host Hostname or IP-Address of the card emulator
     */
    public CardEmulator(String host)
	throws CardEmulatorException {
	this(host, T0_DEFAULT_PORT);
    }

    /**
     * Creates a new card emulator client and establishes a socket connection
     * to the given card emulator.
     *
     * @param host Hostname or IP-Address of the card emulator.
     * @param port Port number of the card emulator.
     */
    public CardEmulator(String host, int port)
	throws CardEmulatorException {
	try
	{
	    Socket socket = new Socket(host, port);
	    BufferedInputStream istm = new BufferedInputStream(socket.getInputStream());
	    BufferedOutputStream ostm = new BufferedOutputStream(socket.getOutputStream());

	    client = CadDevice.getCadClientInstance(CadDevice.PROTOCOL_T1, istm, ostm);

	    /* Power-up the emulator and store the ATR */
	    atr = client.powerUp();
	}
	catch (IOException eio)
	{
	    throw new CardEmulatorException("Failed to establish card emulator connection (I/O error)", eio);
	}
	catch (CadTransportException etx)
	{
	    throw new CardEmulatorException("Failed to get ATR from card emulator", etx);
	}
    }

    /**
     * Disconnect and power down the card emulator
     */
    public void disconnect()
	throws CardEmulatorException {
	try
	{
	    if (client != null)
		client.powerDown();
	}
	catch (Exception e)
	{
	    throw new CardEmulatorException("Problem while disconnecting the card emulator", e);
	}
	finally
	{
	    client = null;
	}
    }

    /**
     * Return a copy of the ATR string received by the emulator upon powerup.:w
     */
    public byte[] getATR()
	{
	    byte[] atrcopy = new byte[atr.length];
	    System.arraycopy(atr, 0, atrcopy, 0, atr.length);
	    return  atrcopy;
	}

    /**
     * Raw APDU exchange using a com.sun.javacard.apduio.Apdu object.
     *
     * @param apdu C-APDU/R-APDU pair to be exchanged.
     */
    public void exchangeApdu(Apdu apdu)
	throws CardEmulatorException {
	try
	{
	    if (client == null)
		throw new CardEmulatorException("No card emulator connected");

	    client.exchangeApdu(apdu);
	}
	catch (IOException eio)
	{
	    throw new CardEmulatorException("Card emulator I/O problem (I/O layer)", eio);
	}
	catch (CadTransportException etx)
	{
	    throw new CardEmulatorException("Card emulator I/O problem (Transport layer)", etx);
	}
    }

    /**
     * Send a command APDU without payload data to the card.
     *
     * @param cla APDU class byte (CLA)
     * @param ins APDU instruction byte (INS)
     * @param p1  APDU parameter byte 1 (P1)
     * @param p2  APDU parameter byte 2 (P2)
     * @return The C-APDU/R-APDU pair exchanged with the card.
     */
    public Apdu exchangeApdu(byte cla, byte ins, byte p1, byte p2)
	throws CardEmulatorException {
	return exchangeApdu(cla, ins, p1, p2, null);
    }

    /**
     * Send a command APDU with payload data to the card.
     *
     * @param cla APDU class byte (CLA)
     * @param ins APDU instruction byte (INS)
     * @param p1  APDU parameter byte 1 (P1)
     * @param p2  APDU parameter byte 2 (P2)
     * @param data APDU payload data
     * @return The C-APDU/R-APDU pair exchanged with the card.
     */
    public Apdu exchangeApdu(byte cla, byte ins, byte p1, byte p2, byte[] data)
	throws CardEmulatorException {
	Apdu apdu = new Apdu();
	apdu.command[Apdu.CLA] = cla;
	apdu.command[Apdu.INS] = ins;
	apdu.command[Apdu.P1] = p1;
	apdu.command[Apdu.P2] = p2;

	if (data != null)
	    apdu.setDataIn(data, data.length);

	exchangeApdu(apdu);

	return apdu;
    }

    /**
     * Send a command APDU with payload data to the card, assert that
     * the response is 0x9000 and return the output data (if any).
     *
     * @param cla APDU class byte (CLA)
     * @param ins APDU instruction byte (INS)
     * @param p1  APDU parameter byte 1 (P1)
     * @param p2  APDU parameter byte 2 (P2)
     * @param data APDU payload data
     * @return The data produced by the card (might be an empty array)
     */
    public byte[] exchange(byte cla, byte ins, byte p1, byte p2, byte[] data)
	throws CardEmulatorException {

	Apdu apdu = exchangeApdu(cla, ins, p1, p2, data);
	checkResponse(apdu, "Operation failed.");

	return apdu.getDataOut();
    }

    /**
     * Install an applet on the JavaCard emulator.
     *
     * This method triggers the installation and instantiation of the applet with the
     * given AID on the JavaCard. Each applet must be installed once on the card before
     * it can be used.
     *
     * @param aid AID of the applet to be installed.
     * @param data Extra user-data to be passed to the applet install method.
     */
    public void installApplet(byte[] aid, byte[] data)
	throws CardEmulatorException {

	/* Select the installet */
	selectApplet(INSTALLER_AID);

	/*
	 * The payload of the INSTALL APDU is:
	 *
	 * [ AIDLEN | AID | DATALEN | DATA ]
	 */
	byte[] installaid = new byte[aid.length + data.length + 2];
	int offset = 0;

	/* AID */
	installaid[offset++] = (byte)aid.length;
	System.arraycopy(aid, 0, installaid, offset, aid.length);
	offset += aid.length;

	/* Applet data */
	installaid[offset++] = (byte)data.length;
	System.arraycopy(data, 0, installaid, offset, data.length);

	/* And submit! */
	checkResponse(exchangeApdu(APDU_INSTALL_CLA, APDU_INSTALL_INS, APDU_INSTALL_P1, APDU_INSTALL_P2, installaid),
		      "Applet installation failed");
    }

    /**
     * Select an applet on the JavaCard emulator.
     *
     * This method selects the active applet on the JavaCard emulator.
     *
     * @param aid AID of the applet to be selected.
     */
    public void selectApplet(byte[] aid)
	throws CardEmulatorException {
	/* And submit! */
    	
	checkResponse(exchangeApdu(APDU_SELECT_CLA, APDU_SELECT_INS, APDU_SELECT_P1, APDU_SELECT_P2, aid),
		      "Applet selection failed");
    }

    /**
     * Checks the SW1/SW2 status bytes of a response APDU and raise an
     * CardEmulatorException if the APDU indicates an error.
     *
     * @param apdu R-APDU to be checked.
     * @param errmessage Error message to be used for the CardEmulatorApduException
     */
    public void checkResponse(Apdu rapdu, String errmessage)
	throws CardEmulatorApduException {
    	if (rapdu.getStatus() != 0x9000)
    	{
    		System.err.println("Operation did not execute - code is: " + rapdu.getStatus());
    		throw new CardEmulatorApduException(errmessage, rapdu);
    	}
    }
}
