package emulator;

import com.sun.javacard.apduio.Apdu;

/**
 * APDU exception thrown by the JavaCard emulator wrapper.
 *
 * @author Johannes Winter <johannes.winter@iaik.tugraz.at>
 */
public final class CardEmulatorApduException extends CardEmulatorException
{
	/**
	 * The C/R-APDU that has been associated with the exception
	 */
	private Apdu apdu;

	/**
	 * Creates a new card emulator exception with a message and an
	 * associated APDU.
	 *
	 * @param message The message to be associated with this exception.
	 * @param apdu C-/R-APDU pair to be associated with this exception.
	 */
	public CardEmulatorApduException(String message, Apdu apdu)
	{
		this(message, apdu, null);
		this.apdu = apdu;
	}

	/**
	 * Creates a new card emulator exception with an exception message and
	 * a cause.
	 *
	 * @param message The message to be associated with this exception.
	 * @param apdu C-/R-APDU pair to be associated with this exception.
	 * @param cause The java.lang.Throwable object to be used as cause.
	 */
	public CardEmulatorApduException(String message, Apdu apdu, Throwable cause)
	{
		super(message + " [C/R-APDU: " + apdu + "]", cause);
		this.apdu = apdu;
	}

	/**
	 * Returns the APDU for this exception
	 */
	public Apdu getApdu()
	{
		return this.apdu;
	}
}
