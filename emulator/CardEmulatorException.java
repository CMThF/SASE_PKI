package emulator;

import com.sun.javacard.apduio.Apdu;

/**
 * Exception thrown by the JavaCard emulator.
 *
 * @author Johannes Winter <johannes.winter@iaik.tugraz.at>
 */
public class CardEmulatorException extends java.lang.Exception
{
	/**
	 * Creates a new card emulator exception with just an exception message.
	 *
	 * @param message The message to be associated with this exception.
	 */
	public CardEmulatorException(String message)
	{
		super(message);
	}

	/**
	 * Creates a new card emulator exception with an exception message and
	 * a cause.
	 *
	 * @param message The message to be associated with this exception.
	 * @param cause The java.lang.Throwable object to be used as cause.
	 */
	public CardEmulatorException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
