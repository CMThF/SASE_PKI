Applied Cryptography and PKI
============================

Security Aspects in Software Development Assignment 2012/13

An exercise in this matter, including 
* Symmetric Encryption (AES)
* HMACs
* RSA Signature Generation and Verification
* RSA Session Key Encipherment
* Simple Certificate Validation (X509)
* Cross Certification
